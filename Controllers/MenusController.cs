﻿using System.Linq;
using ExpressoApi.Data;
using ExpressoApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExpressoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenusController : ControllerBase
    {
        private ExpressoDbContext _expressoDbContext;
        public MenusController(ExpressoDbContext expressoDbContext)
        {
            this._expressoDbContext = expressoDbContext;
        }
       
        [HttpGet]
        public IActionResult GetMenus()
        {
          return Ok(_expressoDbContext.Menus.Include("SubMenus"));
        }

        [HttpGet("{id}")]
        public IActionResult GetMenu(int id)
        {
            Menu menu = _expressoDbContext.Menus.Include("SubMenus").FirstOrDefault(m => m.Id == id);
            if (menu == null)
            {
                return NotFound();
            }

            return Ok(menu);
        }

        [HttpPost]
        [Authorize]
        public IActionResult PostMenu(Menu menu)
        {
            _expressoDbContext.Menus.Add(menu);
            _expressoDbContext.SaveChanges();
            return StatusCode(StatusCodes.Status201Created);
        }
    }
}